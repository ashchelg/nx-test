import { Component } from '@angular/core';

@Component({
  selector: 'nx-ui-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}
