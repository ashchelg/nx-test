import { Component } from '@angular/core';

@Component({
  selector: 'nx-ui-cnp-entry',
  template: `<nx-ui-nx-welcome></nx-ui-nx-welcome>`,
})
export class RemoteEntryComponent {}
